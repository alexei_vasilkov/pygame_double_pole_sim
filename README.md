Double pole simulation with Runge-Kutta fourth order approximation

Equations for accelerations https://books.google.by/books?id=_Z6jBQAAQBAJ&lpg=PA98&ots=xsZNi5efZi&dq=double%20pole%20balancing%20equations&pg=PA100#v=onepage&q&f=false

Runge Kutta approximation method http://lpsa.swarthmore.edu/NumInt/NumIntFourth.html

My code https://bitbucket.org/alexei_vasilkov/pygame_double_pole_sim/src