from double_pole_physics import runge_kutta4
from pylab import *
import numpy as np


def y_deriv(y):
    return -2*y

xlabel('t')
ylabel('y(t)')
title('Runge-Kutta 4th order vs Euler approximation')

step = 0.01
t = arange(0.0, 0.3, step)
ex = 3*np.exp(-2 * np.array(t))
plot(t, ex, color='red', linewidth=10, label='Exact')

#runge kutta approximation method
approx = np.zeros(len(t))
approx[0] = 3
for i in range(1, len(approx)):
    approx[i] = runge_kutta4(approx[i - 1], y_deriv, step)


plot(t, approx, color='black', linewidth=5, label='Runge-Kutta 4th order')

#euler approximation method
approx = np.zeros(len(t))
approx[0] = 3
for i in range(1, len(approx)):
    approx[i] = approx[i - 1] + step*y_deriv(approx[i - 1])

plot(t, approx, color='yellow', linewidth=2, label='Euler')
legend = legend(loc='upper right', shadow=True)
frame = legend.get_frame()
frame.set_facecolor('0.90')
for label in legend.get_texts():
    label.set_fontsize('large')
for label in legend.get_lines():
    label.set_linewidth(1.5)  # the legend line width
savefig("rk4test.png")
show()
